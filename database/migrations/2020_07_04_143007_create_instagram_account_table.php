<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstagramAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instagram_account', function (Blueprint $table) {
            $table->increments('id');
            $table->string('login')->unique();
            $table->string('pass');
            $table->boolean('ban')->default(0);
            $table->dateTime('ban_date_time')->nullable();
            $table->text('banned_why')->nullable();
            $table->integer('proxy_id')->unsigned();
            $table->foreign('proxy_id')->references('id')->on('proxy');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instagram_account');
    }
}
