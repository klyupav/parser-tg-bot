<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddArtisanMarkerPostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('post', function (Blueprint $table) {
            $table->integer('artisan');
            $table->boolean('post_via');
            $table->boolean('post_every');
            $table->float('time');
            $table->integer('count_post');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('post', function (Blueprint $table) {
            $table->dropColumn('artisan');
            $table->dropColumn('post_via');
            $table->dropColumn('post_every');
            $table->dropColumn('time');
            $table->dropColumn('count_post');
        });
    }
}
