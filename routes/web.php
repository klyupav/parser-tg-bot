<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/', 'ParseitController@index');
Route::get('/parseit/youtube', 'ParseitController@parseYoutube');
Route::get('/parseit/posts-from-all-sources', 'ParseitController@parsePostsFromAllSources');
Route::get('/parseit/all-sources-with-threads', 'ParseitController@parseAllSourcesJob');
Route::get('/parseit/source/{id}', 'ParseitController@parsePostsBySourceId');
Route::get('/parseit/source/{sourceId}/account/{accountId}', 'ParseitController@parseInstagramBySourceIdAndAccountId');
Route::post('post-now', 'ParseitController@postNow');
Route::post('/artisan-post-now', 'ParseitController@postArtisanNow');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
