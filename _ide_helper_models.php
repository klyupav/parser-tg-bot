<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\YoutubeApiKey
 *
 * @property int $id
 * @property string $gmail
 * @property string $pass
 * @property string $key
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\YoutubeApiKey newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\YoutubeApiKey newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\YoutubeApiKey query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\YoutubeApiKey whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\YoutubeApiKey whereGmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\YoutubeApiKey whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\YoutubeApiKey whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\YoutubeApiKey wherePass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\YoutubeApiKey whereUpdatedAt($value)
 */
	class YoutubeApiKey extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Source
 *
 * @property int $id
 * @property string $from Donor
 * @property string $src
 * @property int $moderate
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Chanels[] $chanels
 * @property-read int|null $chanels_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Post[] $posts
 * @property-read int|null $posts_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Source newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Source newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Source query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Source whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Source whereFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Source whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Source whereModerate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Source whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Source whereSrc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Source whereUpdatedAt($value)
 */
	class Source extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ArtisanPost
 *
 * @property int $id
 * @property int $source_id
 * @property string $src
 * @property string|null $title
 * @property string|null $description
 * @property string|null $attachments
 * @property string|null $author
 * @property int $moderated
 * @property string|null $publication
 * @property int $have_post
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $artisan
 * @property int $post_via
 * @property int $post_every
 * @property float|null $time
 * @property int $count_post
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Chanels[] $chanels
 * @property-read int|null $chanels_count
 * @property-read \App\Models\Source $source
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtisanPost newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtisanPost newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtisanPost query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtisanPost whereArtisan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtisanPost whereAttachments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtisanPost whereAuthor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtisanPost whereCountPost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtisanPost whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtisanPost whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtisanPost whereHavePost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtisanPost whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtisanPost whereModerated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtisanPost wherePostEvery($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtisanPost wherePostVia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtisanPost wherePublication($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtisanPost whereSourceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtisanPost whereSrc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtisanPost whereTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtisanPost whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtisanPost whereUpdatedAt($value)
 */
	class ArtisanPost extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\InstagramAccount
 *
 * @property int $id
 * @property string $login
 * @property string $pass
 * @property int $ban
 * @property string|null $ban_date_time
 * @property string|null $banned_why
 * @property int $proxy_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $active
 * @property-read \App\Models\Proxy $proxy
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InstagramAccount newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InstagramAccount newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InstagramAccount query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InstagramAccount whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InstagramAccount whereBan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InstagramAccount whereBanDateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InstagramAccount whereBannedWhy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InstagramAccount whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InstagramAccount whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InstagramAccount whereLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InstagramAccount wherePass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InstagramAccount whereProxyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InstagramAccount whereUpdatedAt($value)
 */
	class InstagramAccount extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Proxy
 *
 * @property int $id
 * @property string $address address:port
 * @property string|null $user
 * @property string|null $pass
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\InstagramAccount $instagram_account
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proxy newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proxy newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proxy query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proxy whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proxy whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proxy whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proxy wherePass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proxy whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proxy whereUser($value)
 */
	class Proxy extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Chanels
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $channel_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Source[] $source
 * @property-read int|null $source_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chanels newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chanels newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chanels query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chanels whereChannelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chanels whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chanels whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chanels whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chanels whereUpdatedAt($value)
 */
	class Chanels extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Post
 *
 * @property int $id
 * @property int $source_id
 * @property string $src
 * @property string|null $title
 * @property string|null $description
 * @property string|null $attachments
 * @property string|null $author
 * @property int $moderated
 * @property string|null $publication
 * @property int $have_post
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $artisan
 * @property int $post_via
 * @property int $post_every
 * @property float|null $time
 * @property int $count_post
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Chanels[] $chanels
 * @property-read int|null $chanels_count
 * @property-read \App\Models\Source $source
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereArtisan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereAttachments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereAuthor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereCountPost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereHavePost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereModerated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post wherePostEvery($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post wherePostVia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post wherePublication($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereSourceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereSrc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereUpdatedAt($value)
 */
	class Post extends \Eloquent {}
}

