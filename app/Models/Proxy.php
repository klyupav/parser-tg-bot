<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Proxy extends Model
{
    protected $table = 'proxy';

    protected $ip = null;
    protected $port = null;

    protected $fillable = [
        'address',
        'user',
        'pass'
    ];

    public $timestamps = true;

    public function instagram_account()
    {
        return $this->hasOne(InstagramAccount::class);
    }

    public function ip()
    {
        if (empty($this->ip))
        {
            [$this->ip, $this->port] = explode(':', $this->address);
        }

        return $this->ip;
    }

    public function port()
    {
        if (empty($this->port))
        {
            [$this->ip, $this->port] = explode(':', $this->address);
        }

        return $this->port;
    }

    public static function getFreeProxies()
    {
        $list = [];
        $rows = DB::select('SELECT * FROM `proxy` WHERE `id` NOT IN (SELECT `proxy_id` FROM `instagram_account`)');
        foreach ($rows as $row)
        {
            $list[$row->id] = $row->address;
        }
        return $list;
    }
}
