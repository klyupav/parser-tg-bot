<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'post';

    protected $fillable = [
        'src',
        'title',
        'description',
        'attachments',
        'author',
        'source_id',
        'moderated',
        'have_post',
        'publication'
    ];

    protected $attributes = [
        'moderated' => 0,
        'artisan' => 0
    ];

    protected $attachmentsArray = null;

    public $timestamps = true;

    public static function attachmentsArrayToStr(array $attachments)
    {
        return implode('|', $attachments);
    }

    public function attachmentsToArray()
    {
        if (empty($this->attachmentsArray))
        {
            $this->attachmentsArray = explode('|', $this->attachments);
        }
        return $this->attachmentsArray;
    }

    public function source()
    {
        return $this->belongsTo(Source::class);
    }

    public function chanels()
    {
        return $this->hasManyThrough(Chanels::class, Source::class, 'id', 'id', 'source_id');
    }
}
