<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chanels extends Model
{
    protected $table = 'chanels';

    public function source()
    {
        return $this->belongsToMany(Source::class);
    }
}
