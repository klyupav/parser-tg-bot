<?php

namespace App\Models;

use App\Donors\GeneralDonor;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Source extends Model
{
    protected $table = 'source';

    protected $fillable = [
        'from',
        'name',
        'src',
        'moderate'
    ];

    protected $attributes = [
        'moderate' => 1
    ];

    public $timestamps = true;

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function chanels()
    {
        return $this->belongsToMany(Chanels::class);
    }

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($model) {
            $model->from = GeneralDonor::getDonorTypeByLink($model->src);
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|mixed|static[]
     */
    public static function getAllTypeSources()
    {
//        return Source::groupBy(['from'])->get(['from']);
        return Cache::remember('getAllTypeSources', now()->addHour(1), function ()
        {
            return Source::groupBy(['from'])->get(['from']);
        });
    }

    /**
     * @param string $type
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|mixed|static[]
     */
    public static function getAllSourcesByType(string $type)
    {
//        return Source::where(['from' => $type])->get();
        return Cache::remember('getAllSourcesByType'.$type, now()->addHour(1), function () use ($type)
        {
            return Source::where(['from' => $type])->get();
        });
    }
}
