<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InstagramAccount extends Model
{
    private $try = 0;

    protected $table = 'instagram_account';

    protected $fillable = [
        'login',
        'pass',
        'ban',
        'ban_date_time',
        'banned_why',
        'proxy_id',
        'active',
    ];

    public $timestamps = true;

    public function proxy()
    {
        return $this->belongsTo(Proxy::class);
    }

    public function tries()
    {
        return $this->try;
    }

    public function increaseTry()
    {
        $this->try++;
    }
}
