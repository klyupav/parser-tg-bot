<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class YoutubeApiKey extends Model
{
    private $try = 0;

    protected $table = 'youtube_api_key';

    protected $fillable = [
        'gmail',
        'pass',
        'key'
    ];

    public $timestamps = true;

    public function tries()
    {
        return $this->try;
    }

    public function increaseTry()
    {
        $this->try++;
    }
}
