<?php
/**
 * Created by PhpStorm.
 * User: klyupav
 * Date: 24.06.20
 * Time: 17:24
 */

namespace App\Donors;


/**
 * Class ParseitDonor
 * @package App\Donors
 */
class GeneralDonor extends AbstractDonor
{
    /**
     * @param string $source
     * @return \App\Models\Post[]
     */
    public function getPosts(string $source)
    {
        $donor = $this->getInstanceDonor($source);
        return $donor->getPosts($source);
    }

    private function getInstanceDonor(string $source)
    {
        $donorType = self::getDonorTypeByLink($source);

        switch ($donorType)
        {
            case 'Instagram':
                return Instagram::getInstance();
                break;
            case 'Youtube':
                return Youtube::getInstance();
                break;

        }
    }

    /**
     * @param string $link
     * @return string
     * @throws \Exception
     */
    public static function getDonorTypeByLink(string $link)
    {
        if (strpos($link, 'instagram.com/') !== false)
        {
            return Instagram::getInstance()->getType();
        }
        elseif (strpos($link, 'youtube.com/') !== false)
        {
            return Youtube::getInstance()->getType();
        }
        else
        {
            throw new \Exception('Invalid source: ' . $link);
        }
    }
}