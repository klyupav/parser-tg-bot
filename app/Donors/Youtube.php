<?php

namespace App\Donors;

use App\Models\Post;
use App\Models\YoutubeApiKey;


/**
 * Class Instagram
 * @package App\Donors
 * @var string $source
 * @var string $type
 */

class Youtube extends AbstractDonor
{
    private $api;
    private $youtubeKeys;
    private $apiKey;

    /**
     * Youtube constructor.
     */
    public function __construct()
    {
        $this->type = 'Youtube';
        $this->youtubeKeys = YoutubeKeys::getInstance();
        $this->apiKey = $this->youtubeKeys->next();
        $this->api = new \Alaouy\Youtube\Youtube($this->apiKey->key);
    }

    public function getPosts(string $source)
    {
        if (empty($source))
        {
            throw new \Exception('Empty param - source');
        }

        $findSource = $this->findOrCreateSource($source);

        $chanelId = $this->getChanelId($source);

        $videoList = $this->searchChannelVideos('', $chanelId, 1, 'date');

        $posts = [];

        if (empty($videoList))
        {
            return [new Post()];
        }

        $videoList = array_reverse($videoList);

        foreach ($videoList as $video)
        {
            $src = "https://www.youtube.com/watch?v={$video->id->videoId}";
            $postValues = [
                'src' => $src,
                'title' => $video->snippet->title,
                'description' => $video->snippet->description,
                'attachments' => $src,
                'author' => $video->snippet->channelTitle,
                'source_id' => $findSource->id
            ];
            if (!$findSource->moderate)
            {
                $postValues['moderated'] = 1;
            }

            $post = $this->updateOrCreatePost($postValues);
            $posts[] = $post;
        }

        return empty($posts) ? [new Post()] : $posts;
    }

    private function searchChannelVideos(string $q, string $channelId, int $maxResults = 10, string $order = null, $tryAgain = false)
    {
        try
        {
            if (!$tryAgain)
            {
                $this->apiKey = $this->youtubeKeys->next();
                $this->api->setApiKey($this->apiKey->key);
            }

            $videoList = $this->api->searchChannelVideos($q, $channelId, $maxResults, $order);
        }
        catch (\Exception $exception)
        {
            $this->apiKey->increaseTry();

            if ($this->apiKey->tries() >= 3)
            {
                $tryAgain = false;
                $this->youtubeKeys->remove($this->apiKey, $exception);
            }
            else
            {
                $tryAgain = true;
                sleep(5);
            }

            $videoList = $this->searchChannelVideos($q, $channelId, $maxResults, $order, $tryAgain);
        }

        return $videoList;
    }

    private function getChanelId(string $source)
    {
        try
        {
            $chanel = $this->api->getChannelFromURL($source);
            return $chanel->id;
        }
        catch (\Exception $exception)
        {
            $html = $this->api->api_get($source, []);

            if (preg_match("%<meta itemprop=\"channelId\" content=\"([^\"]+)\"%uis", $html, $match))
            {
                $chanelId = $match[1];
                return $chanelId;
            }
            else
            {
                throw new \Exception($exception->getMessage());
            }
        }
    }
}