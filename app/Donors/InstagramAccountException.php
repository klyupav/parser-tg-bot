<?php
/**
 * Created by PhpStorm.
 * User: klyupav
 * Date: 05.07.20
 * Time: 0:21
 */

namespace App\Donors;


class InstagramAccountException extends \Exception
{
    public function __construct($message = "", $code = 500, $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}