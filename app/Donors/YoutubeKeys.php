<?php
/**
 * Created by PhpStorm.
 * User: klyupav
 * Date: 04.07.20
 * Time: 22:45
 */

namespace App\Donors;


use App\Models\YoutubeApiKey;
use Illuminate\Database\Eloquent\Collection;

class YoutubeKeys
{
    private static $instances = [];

    protected $apiKeys;
    protected $usedKeys;

    /**
     * YoutubeKeys constructor.
     * @param Collection|null $keys
     */
    public function __construct(Collection $keys = null)
    {
        $this->usedKeys = YoutubeApiKey::whereId(0)->get();
        if (is_null($keys))
        {
            $this->apiKeys = static::getAllWorks();
        }
        else
        {
            $this->apiKeys = $keys;
        }
    }

    /**
     * @return YoutubeKeys
     */
    public static function getInstance()
    {
        $cls = static::class;
        if (!isset(self::$instances[$cls]))
        {
            self::$instances[$cls] = new static;
        }

        return self::$instances[$cls];
    }

    /**
     * @return YoutubeApiKey
     * @throws \Exception
     */
    public function next()
    {
        if ($this->apiKeys->isEmpty())
        {
            if ($this->usedKeys->isEmpty())
            {
                throw new \Exception('No free api keys');
            }

            $this->apiKeys = $this->usedKeys;
            $this->usedKeys = YoutubeApiKey::whereId(0)->get();
        }

        $apiKey = $this->apiKeys->shift();
        $this->usedKeys->push($apiKey);

        return $apiKey;
    }

    /**
     * @param YoutubeApiKey $model
     * @param \Exception|null $exception
     */
    public function remove(YoutubeApiKey $model, \Exception $exception = null)
    {
        foreach ($this->usedKeys as $key => $usedApiKey)
        {
            if ($usedApiKey->gmail === $model->gmail)
            {
                $this->usedKeys->forget([$key]);
                break;
            }
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function getAllWorks()
    {
        return YoutubeApiKey::all();
    }
}