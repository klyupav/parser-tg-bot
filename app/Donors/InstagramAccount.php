<?php
/**
 * Created by PhpStorm.
 * User: klyupav
 * Date: 04.07.20
 * Time: 22:45
 */

namespace App\Donors;


use Illuminate\Database\Eloquent\Collection;

class InstagramAccount
{
    private static $instances = [];

    protected $accounts;
    protected $usedAccounts;

    /**
     * InstagramAccount constructor.
     */
    public function __construct(Collection $accounts = null)
    {
        $this->usedAccounts = \App\Models\InstagramAccount::whereId(0)->get();
        if (is_null($accounts))
        {
            $this->accounts = static::getAllWorks();
        }
        else
        {
            $this->accounts = $accounts;
        }
    }

    /**
     * @return InstagramAccount
     */
    public static function getInstance()
    {
        $cls = static::class;
        if (!isset(self::$instances[$cls]))
        {
            self::$instances[$cls] = new static;
        }

        return self::$instances[$cls];
    }

    /**
     * @return \App\Models\InstagramAccount
     * @throws InstagramAccountException
     */
    public function next()
    {
        if ($this->accounts->isEmpty())
        {
            if ($this->usedAccounts->isEmpty())
            {
                throw new InstagramAccountException('No works Instagram accounts');
            }

            $this->accounts = $this->usedAccounts;
            $this->usedAccounts = \App\Models\InstagramAccount::whereId(0)->get();
        }

        $account = $this->accounts->shift();
        $this->usedAccounts->push($account);
        $account->update(['ban' => 0, 'ban_date_time' => null, 'banned_why' => null]);

        return $account;
    }

    /**
     * @param \App\Models\InstagramAccount $model
     * @param \Exception|null $exception
     */
    public function remove(\App\Models\InstagramAccount $model, \Exception $exception = null)
    {
        foreach ($this->usedAccounts as $key => $usedAccount)
        {
            if ($usedAccount->login === $model->login)
            {
                $this->usedAccounts->forget([$key]);
                break;
            }
        }
        $model->update([
            'ban' => 1,
            'ban_date_time' => now(),
            'banned_why' => empty($exception) ? "" : $exception->getMessage(),
        ]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function getAllWorks()
    {
        return \App\Models\InstagramAccount::where(['active' => 1])
            ->where(function ($query) {
                $query->where(['ban' => 0])->orWhere('ban_date_time', '<', now()->subHours(2));
            })
            ->get();
    }
}