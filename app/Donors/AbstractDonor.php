<?php
/**
 * Created by PhpStorm.
 * User: klyupav
 * Date: 24.06.20
 * Time: 16:21
 */

namespace App\Donors;


use App\Models\Post;
use App\Models\Source;

abstract class AbstractDonor
{
    protected $source;
    protected $type;
    private static $instances = [];

    /**
     * @param string $source
     * @return Post[]
     */
    abstract public function getPosts(string $source);

    /**
     * @return AbstractDonor
     */
    public static function getInstance()
    {
        $cls = static::class;
        if (!isset(self::$instances[$cls])) {
            self::$instances[$cls] = new static;
        }

        return self::$instances[$cls];
    }

    /**
     * @param string $source
     * @return $this|\Illuminate\Database\Eloquent\Model|null|object|static
     */
    protected function findOrCreateSource(string $source)
    {
        $findSource = Source::where(['src' => $source])->first();
        if (!$findSource)
        {
            $findSource = Source::create([
                'from' => $this->type,
                'src' => $source,
                'name' => $source,
            ]);
        }

        return $findSource;
    }

    /**
     * @param array $postValues
     * @return $this|\Illuminate\Database\Eloquent\Model|null|object|static
     */
    protected function updateOrCreatePost(array $postValues)
    {
        if ($post = Post::whereSrc($postValues['src'])->first())
        {
            $post->update($postValues);
        }
        else
        {
            $post = Post::create($postValues);
        }

        return $post;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}