<?php
/**
 * Created by PhpStorm.
 * User: klyupav
 * Date: 24.06.20
 * Time: 15:07
 */

namespace App\Donors;



use App\Models\Post;
use App\Models\Source;
use InstagramScraper\Exception\InstagramException;
use InstagramScraper\Exception\InstagramNotFoundException;
use Phpfastcache\CacheManager;

/**
 * Class Instagram
 * @package App\Donors
 * @var string $source
 * @var string $type
 */

class Instagram extends AbstractDonor
{
    private $instagram;
    private $psr16;
    private $account;
    private $instagramAccounts;
    private $withThatAccount = null;


    /**
     * Instagram constructor.
     * @param \App\Models\InstagramAccount|null $instagramAccount
     * @throws \Phpfastcache\Exceptions\PhpfastcacheDriverCheckException
     * @throws \Phpfastcache\Exceptions\PhpfastcacheInvalidConfigurationException
     * @throws \Phpfastcache\Exceptions\PhpfastcacheLogicException
     * @throws \ReflectionException
     */
    public function __construct(InstagramAccount $accounts = null, \App\Models\InstagramAccount $withThatAccount = null)
    {
        $this->type = 'Instagram';
        $this->psr16 = new \Phpfastcache\Helper\Psr16Adapter('Files',
            new \Phpfastcache\Config\ConfigurationOption([
                'defaultTtl' => 43200
            ]) // Auth cache 1 day
        );

        if (is_null($accounts))
        {
            $this->instagramAccounts = InstagramAccount::getInstance();
        }
        else
        {
            $this->instagramAccounts = $accounts;
        }

        $this->withThatAccount = $withThatAccount;
    }


    /**
     * @param string $source
     * @param \App\Models\InstagramAccount|null $account
     * @return Post[]
     * @throws \Exception
     * @throws \Phpfastcache\Exceptions\PhpfastcacheDriverCheckException
     * @throws \Phpfastcache\Exceptions\PhpfastcacheInvalidConfigurationException
     * @throws \Phpfastcache\Exceptions\PhpfastcacheLogicException
     * @throws \ReflectionException
     */
    public function getPosts(string $source)
    {
        if (empty($source))
        {
            throw new \Exception('Empty param - source');
        }

        $findSource = $this->findOrCreateSource($source);

        $username = $this->getUsername($source);

        $nonPrivateAccountMedias =

            empty($this->withThatAccount) ?

                $this->getMedias($username, 1) :

                $this->getMediasWithAccount($this->withThatAccount, $username, 1);

        $posts = [];

        if (empty($nonPrivateAccountMedias))
        {
            return [new Post()];
        }

        foreach ($nonPrivateAccountMedias as $accountMedia)
        {
            $postType = $accountMedia->getType();
            $postValues = [
                'src' => $accountMedia->getLink(),
                'title' => '',
                'description' => $accountMedia->getCaption(),
                'attachments' => '',
                'author' => $username,
                'source_id' => $findSource->id
            ];
            if (!$findSource->moderate)
            {
                $postValues['moderated'] = 1;
            }
            switch ($postType)
            {
                case 'sidecar':
                    $attachments = $this->getAttachmentsSidecar($accountMedia);
                    $postValues['attachments'] = Post::attachmentsArrayToStr($attachments);
                    break;
                case 'video':
                    $postValues['attachments'] = $accountMedia->getVideoStandardResolutionUrl();
                    break;
                case 'image':
                    $postValues['attachments'] = $accountMedia->getImageHighResolutionUrl();
                    break;
                default:
                    throw new \Exception('Unknown post type: ' . $postType);
                    break;
            }

            $post = $this->updateOrCreatePost($postValues);
            $posts[] = $post;
        }

        return empty($posts) ? [new Post()] : $posts;
    }

    /**
     * @param string $username
     * @param int $count
     * @return \InstagramScraper\Model\Media[]
     * @throws \Phpfastcache\Exceptions\PhpfastcacheDriverCheckException
     * @throws \Phpfastcache\Exceptions\PhpfastcacheInvalidConfigurationException
     * @throws \Phpfastcache\Exceptions\PhpfastcacheLogicException
     * @throws \ReflectionException
     */
    private function getMedias(string $username, $count = 20, $tryAgain = false)
    {
        try
        {
            if (!$tryAgain)
            {
                $this->account = $this->instagramAccounts->next();
                $this->setAccount($this->account);
            }

            $nonPrivateAccountMedias = $this->instagram->getMedias($username, $count);
        }
        catch (\Exception $exception)
        {
            $classException = get_class($exception);
            if ($classException == InstagramAccountException::class)
            {
                throw new InstagramAccountException($exception->getMessage());
            }

            if ($classException == InstagramNotFoundException::class || $classException == InstagramException::class)
            {
                return null;
            }

            $this->account->increaseTry();

            if ($this->account->tries() >= 3)
            {
                $tryAgain = false;
                $this->instagramAccounts->remove($this->account, $exception);
            }
            else
            {
                $tryAgain = true;
                sleep(5);
            }

            $nonPrivateAccountMedias = $this->getMedias($username, $count, $tryAgain);
        }

        return $nonPrivateAccountMedias;
    }

    /**
     * @param \App\Models\InstagramAccount $account
     * @param string $username
     * @param int $count
     * @return \InstagramScraper\Model\Media[]
     */
    private function getMediasWithAccount(\App\Models\InstagramAccount $account, string $username, $count = 20)
    {
        try
        {
            $this->account = $account;
            $this->setAccount($this->account);
            $nonPrivateAccountMedias = $this->instagram->getMedias($username, $count);
        }
        catch (\Exception $exception)
        {
            $classException = get_class($exception);
            if ($classException == InstagramNotFoundException::class || $classException == InstagramException::class)
            {
                return null;
            }
            throw new $classException($exception);
        }

        return $nonPrivateAccountMedias;
    }

    private function setAccount(\App\Models\InstagramAccount $instagramAccount)
    {
        $this->account = $instagramAccount;

        \InstagramScraper\Instagram::disableProxy();

        if (!empty($this->account->proxy->user))
        {
            \InstagramScraper\Instagram::setProxy([
                'address' => $this->account->proxy->ip(),
                'port'    => $this->account->proxy->port(),
                'tunnel'  => true,
                'timeout' => 30,
                'auth' => [
                    'user' => $this->account->proxy->user,
                    'pass' => $this->account->proxy->pass,
                    'method' => CURLAUTH_BASIC
                ],
            ]);
        }
        else
        {
            \InstagramScraper\Instagram::setProxy([
                'address' => $this->account->proxy->ip(),
                'port'    => $this->account->proxy->port(),
                'tunnel'  => true,
                'timeout' => 30,
            ]);
        }

        $this->instagram = \InstagramScraper\Instagram::withCredentials(
            $this->account->login,
            $this->account->pass,
            $this->psr16
        );

        $this->instagram->login();
        $this->instagram->saveSession();

        return $this->account;
    }

    /**
     * @param \InstagramScraper\Model\Media $media
     * @return array
     * @throws \Exception
     */
    private function getAttachmentsSidecar(\InstagramScraper\Model\Media $media)
    {
        $attachments = [];

        foreach ($media->getSidecarMedias() as $sidecarMedia)
        {
            $sidecarMediaType = $sidecarMedia->getType();
            switch ($sidecarMediaType)
            {
                case 'image':
                    $attachments[] = $sidecarMedia->getImageHighResolutionUrl();
                    break;
                case 'video':
                    $attachments[] = $sidecarMedia->getVideoStandardResolutionUrl();
                    break;
                default:
                    throw new \Exception('Unknown sidecar media type: ' . $sidecarMediaType);
                    break;
            }
        }

        return $attachments;
    }

    /**
     * @param string $source
     * @return string
     * @throws \Exception
     */
    private function getUsername(string $source)
    {
        if (preg_match("%instagram\.com/([^\/]+)/%uis", $source, $match))
        {
            return $match[1];
        }

        throw new \Exception('Invalid source: ' . $source);
    }
}