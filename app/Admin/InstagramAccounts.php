<?php

use App\Models\InstagramAccount;
use App\Models\Proxy;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel( InstagramAccount::class, function ( ModelConfiguration $model )
{
    $model->setTitle( 'Инстаграм аккаунты' );

    $model->onDisplay( function ()
    {
        $display = AdminDisplay::datatablesAsync()->setHtmlAttribute( 'class', 'table-primary table-hover' );
        $display->with('proxy');

        $display->setColumnFilters([
            null,
            null,
            AdminColumnFilter::text()->setPlaceholder('Логин')->setOperator(\SleepingOwl\Admin\Contracts\Display\Extension\FilterInterface::CONTAINS)
        ]);

        $display->setColumns(
            AdminColumn::text( 'id' )->setLabel( 'ID' )->setWidth( '10px' ),
            AdminColumnEditable::checkbox('active')->setLabel('ВКЛ.'),
            AdminColumn::link( 'login' )->setLabel( 'Логин' )->setWidth( '100px' ),
            AdminColumn::text( 'pass' )->setLabel( 'Пароль' )->setWidth( '100px' ),
            AdminColumn::custom('В бане', function(\Illuminate\Database\Eloquent\Model $model) {
                return $model->ban ? 'Да' : 'Нет';
            })->setWidth('100px'),
            AdminColumn::custom('Время в бане', function(\Illuminate\Database\Eloquent\Model $model) {
                if (empty($model->ban_date_time))
                {
                    return null;
                }
                else
                {
                    $bannedDateTime = \Carbon\Carbon::parse($model->ban_date_time);
                    return now()->diffForHumans($bannedDateTime);
                }
            })->setWidth('100px'),
            AdminColumn::custom('Причина бана', function(\Illuminate\Database\Eloquent\Model $model) {
                if (empty($model->banned_why))
                {
                    return null;
                }
                else
                {
                    return \substr($model->banned_why, 0, 300)."...";
                }
            })->setWidth('100px')
//            AdminColumn::text( 'banned_why' )->setLabel( 'Причина бана' )->setWidth( '100px' )
        )->paginate( 10 );

        return $display;
    } );

    $model->onCreateAndEdit( function ( $id = null )
    {
        $freeProxyList = Proxy::getFreeProxies();
        if (!empty($id))
        {
            $model = InstagramAccount::find($id);
            $freeProxyList[$model->proxy->id] = $model->proxy->address;
        }
        $panel = AdminForm::panel()
            ->addBody(
                AdminFormElement::text( 'login', 'Логин' ),
                AdminFormElement::text( 'pass', 'Пароль' ),
                AdminFormElement::select('proxy_id', 'Прокси')->setOptions($freeProxyList)
            );
        $panel->getButtons()->hideDeleteButton();
        return $panel ;
    } );
} );