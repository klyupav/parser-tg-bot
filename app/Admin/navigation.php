<?php

use SleepingOwl\Admin\Navigation\Page;


return [
    [
        'title' => 'Dashboard',
        'icon'  => 'fas fa-tachometer-alt',
        'url'   => route('admin.dashboard'),
    ],

    [
        'title' => 'Information',
        'icon'  => 'fas fa-info-circle',
        'url'   => route('admin.information'),
    ],

    AdminSection::addMenuPage(\App\Models\Post::class)->setIcon('fas fa-apple'),
    AdminSection::addMenuPage(\App\Models\ArtisanPost::class)->setIcon('fas fa-apple'),
    AdminSection::addMenuPage(\App\Models\Source::class)->setIcon('fas fa-apple'),
    AdminSection::addMenuPage(\App\Models\Chanels::class)->setIcon('fas fa-apple'),
    AdminSection::addMenuPage(\App\Models\InstagramAccount::class)->setIcon('fas fa-apple'),
    AdminSection::addMenuPage(\App\Models\Proxy::class)->setIcon('fas fa-apple'),
    AdminSection::addMenuPage(\App\Models\YoutubeApiKey::class)->setIcon('fas fa-apple'),
];
