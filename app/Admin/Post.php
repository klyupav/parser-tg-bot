<?php

use App\Models\Post;
use App\Models\Chanels;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel( Post::class, function ( ModelConfiguration $model )
{
    $model->setTitle( 'Посты' );
    $model->onDisplay( function ()
    {
        $display = AdminDisplay::datatablesAsync()->setHtmlAttribute( 'class', 'table-primary table-hover' );
        $display->setColumnFilters([
            null,
            AdminColumnFilter::text('author')->setPlaceholder('Автор'),
            null,
            AdminColumnFilter::text('custom_filter')->setCallback(function($query, $value) {
                $query->where('myField', $value);
            }),
            null,
            null,
        ]);
        $display->setColumns(
            AdminColumn::text( 'id' )->setLabel( 'ID' )->setWidth( '10px' ),
            AdminColumn::text( 'author' )->setLabel( 'Автор' )->setWidth( '100px' ),
            AdminColumn::text( 'description' )->setLabel( 'Текст описания' )->setWidth( '300px' ),
            AdminColumn::custom('Канал', function(\Illuminate\Database\Eloquent\Model $model) {
                $chanels= $model->source->chanels;
                $name = '';
                foreach ($chanels as $chanel){
                    $name .= $chanel->title;
                }
                return $name;
            })->setWidth('150px')->setFilterCallback(function($column, $query, $search) {
                if (isset($search) && !empty($search))
                {
                    $chanel = DB::table('chanels')->where('title', $search)->first();
                    $sources =  DB::table('chanels_source')->where('chanels_id', $chanel->id)
                        ->leftJoin('source', 'source.id', '=', 'chanels_source.source_id')
                        ->get();

                    foreach ($sources as $source){
                        $source_id[] = $source->source_id;
                    }
                  return   $posts = $query->whereIn('source_id', $source_id)->get();
                }

            }),
            AdminColumn::custom('Модерация', function(\Illuminate\Database\Eloquent\Model $model) {
                $name = 'Нет';
                if ($model->moderated == 1){
                    $name = 'Поститься';
                }elseif ($model->source->moderate){
                    $name = 'Поститься';
                }
                return $name;
            })->setWidth('150px')->setFilterCallback(function($column, $query, $search) {
                echo $search;
            }),
//            AdminColumn::custom('Публикация', function(\Illuminate\Database\Eloquent\Model $model) {
//                $name = 'Нет';
//                if ($model->have_post == 1){
//                    $name = 'запощен';
//                }
//                return $name;
//            })->setWidth('150px')->setFilterCallback(function($column, $query, $search) {
//                echo $search;
//            })
            AdminColumnEditable::checkbox('have_post')->setLabel('Публикация')
        )->paginate( 10 );
        return $display;
    } );
    $model->created(function(ModelConfiguration $model, Post $post) {
        $images = json_decode($post->attachments);
        $a = [];
        foreach ($images as $image){
            $a[] = asset($image).'?'.\Illuminate\Support\Str::random(40);
        }
        $post->attachments = $post->attachmentsArrayToStr($a);
        $post->save();
    });
    $model->onCreate( function ( $id = null )
    {
        $panel = AdminForm::panel()
            ->addBody(
//                AdminFormElement::view('admin.images', $data = [], $callback = null),
                AdminFormElement::images('attachments', 'Картинки')->storeAsJson(),
                AdminFormElement::text( 'author', 'Автор' ),
                AdminFormElement::textarea( 'description', 'Текст описания' ),
                AdminFormElement::checkbox  ( 'moderated', 'Пропускать' ),
                AdminFormElement::datetime  ( 'publication', 'Время поста' ),
                AdminFormElement::select('source_id', 'Ресурс', \App\Models\Source::class)->setDisplay('src'),
                AdminFormElement::hidden('src')->setDefaultValue(\Illuminate\Support\Str::random(20)),
                AdminFormElement::hidden('artisan')->setDefaultValue(0)
            )
        ;
        $panel ->getButtons()->hideDeleteButton();
        return $panel ;
    } );
    $model->onEdit( function ( $id = null )
    {
        $panel = AdminForm::panel()
            ->addBody(
                AdminFormElement::view('admin.images', $data = [], $callback = null),
                AdminFormElement::text( 'author', 'Автор' ),
                AdminFormElement::textarea( 'description', 'Текст описания' ),
                AdminFormElement::checkbox  ( 'moderated', 'Пропускать' ),
                AdminFormElement::datetime  ( 'publication', 'Время поста' ),
                AdminFormElement::view('admin.post_now', $data = [], $callback = null)

            )
        ;
        $panel ->getButtons()->hideDeleteButton();
        return $panel ;
    } );
} );