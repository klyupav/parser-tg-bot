<?php

use App\Models\Post;
use \App\Models\ArtisanPost;
use App\Models\Chanels;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel( ArtisanPost::class, function ( ModelConfiguration $model )
{
    $model->setTitle( 'Рукотворные посты' );
    $model->onDisplay( function ()
    {
        $display = AdminDisplay::datatablesAsync()->setHtmlAttribute( 'class', 'table-primary table-hover' );
        $display->setApply(function($query) {
            $query->where('artisan', 1);
        });
        $display->setColumnFilters([
            null,
            AdminColumnFilter::text('author')->setPlaceholder('Автор'),
            null,
            AdminColumnFilter::text('custom_filter')->setCallback(function($query, $value) {
                $query->where('myField', $value);
            }),
            null,
            null,
        ]);
        $display->setColumns(
            AdminColumn::text( 'id' )->setLabel( 'ID' )->setWidth( '50px' ),
            AdminColumn::text( 'author' )->setLabel( 'Автор' )->setWidth( '200px' ),
            AdminColumn::link( 'description' )->setLabel( 'Текст описания' )->setWidth( '300px' ),
            AdminColumn::custom('Канал', function(\Illuminate\Database\Eloquent\Model $model) {
                $chanels = $model->source->chanels;
                $name = '';
                foreach ($chanels as $chanel){
                    $name .= $chanel->title;
                }
                return $name;
            })->setWidth('150px')->setFilterCallback(function($column, $query, $search) {
                if (isset($search) && !empty($search))
                {
                    $chanel = DB::table('chanels')->where('title', $search)->first();
                    $sources =  DB::table('chanels_source')->where('chanels_id', $chanel->id)
                        ->leftJoin('source', 'source.id', '=', 'chanels_source.source_id')
                        ->get();

                    foreach ($sources as $source){
                        $source_id[] = $source->source_id;
                    }
                    return   $posts = $query->whereIn('source_id', $source_id)->get();
                }

            }),
            AdminColumn::custom('Модерация', function(\Illuminate\Database\Eloquent\Model $model) {
                $name = 'Нет';
                if ($model->moderated == 1){
                    $name = 'Поститься';
                }elseif ($model->source->moderate){
                    $name = 'Поститься';
                }
                return $name;
            })->setWidth('150px')->setFilterCallback(function($column, $query, $search) {
                echo $search;
            }),
            AdminColumn::text('count_post')->setLabel('Кол-во публикаций'),
            AdminColumnEditable::checkbox('have_post')->setLabel('Публикация'),
            AdminColumnEditable::checkbox('post_via')->setLabel('Постить через'),
            AdminColumnEditable::checkbox('post_every')->setLabel('Постить каждые'),
            AdminColumnEditable::text('time')->setLabel('Время')
        )->paginate( 10 );
        return $display;
    } );
    $model->created(function(ModelConfiguration $model, ArtisanPost $post) {
        $images = json_decode($post->attachments);
        $a = [];
        foreach ($images as $image){
            $a[] = asset($image).'?'.\Illuminate\Support\Str::random(40);
        }
        $post->attachments = $post->attachmentsArrayToStr($a);
        $post->publication = \Carbon\Carbon::now();
        $post->save();
    });
    $model->onCreate( function ( $id = null )
    {
        $panel = AdminForm::panel();
        $panel->addHeader(AdminFormElement::columns()
            ->addColumn([
                AdminFormElement::text( 'author', 'Автор' ),
            ], 3)
            ->addColumn([
                AdminFormElement::images('attachments', 'Картинки')->storeAsJson(),
            ], 9)
            ->addColumn([
                AdminFormElement::textarea( 'description', 'Текст описания' ),
                AdminFormElement::checkbox  ( 'moderated', 'Пропускать' ),
            ], 12)
            ->addColumn([
                AdminFormElement::datetime  ( 'publication', 'Время поста' ),
            ], 3)
            ->addColumn([
                AdminFormElement::number  ( 'time', 'Время' )->setStep(0.01),

            ], 12)
            ->addColumn([
                AdminFormElement::checkbox  ( 'post_via', 'Постить через' ),
            ], 3)
            ->addColumn([
                AdminFormElement::checkbox  ( 'post_every', 'Постить каждые' ),
            ], 3)
            ->addColumn([
                AdminFormElement::select('source_id', 'Ресурс', \App\Models\Source::class)->setDisplay(function ($model) {
                    return $model->name . ' | ' . $model->from;
                }),
                AdminFormElement::hidden('src')->setDefaultValue(\Illuminate\Support\Str::random(20)),
                AdminFormElement::hidden('artisan')->setDefaultValue(1)
            ], 12)
        );
        $panel ->getButtons()->hideDeleteButton();
        return $panel ;
    } );
    $model->onEdit( function ( $id = null )
    {
        $panel = AdminForm::panel()
            ->addBody(
                AdminFormElement::view('admin.images', $data = [], $callback = null),
                AdminFormElement::text( 'author', 'Автор' ),
                AdminFormElement::textarea( 'description', 'Текст описания' ),
                AdminFormElement::checkbox  ( 'moderated', 'Пропускать' ),
                AdminFormElement::datetime  ( 'publication', 'Время поста' ),
                AdminFormElement::view('admin.posrt_now_artisan', $data = [], $callback = null)

            );
        $panel ->getButtons()->hideDeleteButton();
        return $panel ;
    } );
} );