<?php

use App\Models\Chanels;
use App\Models\Source;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel( Chanels::class, function ( ModelConfiguration $model )
{
    $model->setTitle( 'ТГ каналы' );
    $model->onDisplay( function ()
    {
        $display = AdminDisplay::datatablesAsync()->setHtmlAttribute( 'class', 'table-primary table-hover' );
        $display->with('source');
        $display->setColumns(
            AdminColumn::text( 'id' )->setLabel( 'ID' )->setWidth( '10px' ),
            AdminColumn::text( 'title' )->setLabel( 'Название' )->setWidth( '100px' ),
            AdminColumn::link( 'channel_id' )->setLabel( 'id канала' )->setWidth( '100px' )
        )->paginate( 10 );
        return $display;
    } );
    $model->onCreateAndEdit( function ( $id = null )
    {
        $panel = AdminForm::panel()
            ->addBody(
                AdminFormElement::text( 'channel_id', 'id канала' ),
                AdminFormElement::text( 'title', 'Название' ),
				AdminFormElement::multiselect('source', 'Ресурсы', Source::class)->setDisplay(function ($model) {
									return $model->name . ' | ' . $model->from;
								})
            );
        $panel ->getButtons()->hideDeleteButton();
        return $panel ;
    } );
} );