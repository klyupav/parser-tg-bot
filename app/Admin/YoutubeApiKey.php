<?php

use App\Models\InstagramAccount;
use App\Models\Proxy;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel( \App\Models\YoutubeApiKey::class, function ( ModelConfiguration $model )
{
    $model->setTitle( 'Youtube API keys' );

    $model->onDisplay( function ()
    {
        $display = AdminDisplay::datatablesAsync()->setHtmlAttribute( 'class', 'table-primary table-hover' );

        $display->setColumns(
            AdminColumn::text( 'id' )->setLabel( 'ID' )->setWidth( '10px' ),
            AdminColumn::link( 'gmail' )->setLabel( 'GMail' )->setWidth( '100px' ),
            AdminColumn::text( 'pass' )->setLabel( 'Пароль' )->setWidth( '100px' ),
            AdminColumn::text( 'key' )->setLabel( 'API key' )->setWidth( '100px' )
        )->paginate( 10 );

        return $display;
    } );

    $model->onCreateAndEdit( function ( $id = null )
    {
        $panel = AdminForm::panel()
            ->addBody(
                AdminFormElement::text( 'gmail', 'Логин' ),
                AdminFormElement::text( 'pass', 'Пароль' ),
                AdminFormElement::text( 'key', 'API key' )
            );
        $panel->getButtons()->hideDeleteButton();
        return $panel ;
    } );
} );