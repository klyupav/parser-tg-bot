<?php

use App\Models\Source;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel( Source::class, function ( ModelConfiguration $model )
{
    $model->setTitle( 'Ресурсы' );
    $model->onDisplay( function ()
    {
        $display = AdminDisplay::datatablesAsync()->setHtmlAttribute( 'class', 'table-primary table-hover' );
		$display->setColumnFilters([
            null,
            AdminColumnFilter::text('name')->setPlaceholder('Название')->setOperator(\SleepingOwl\Admin\Contracts\Display\Extension\FilterInterface::CONTAINS),
            null,
            null
        ]);
        $display->setColumns(
            AdminColumn::text( 'id' )->setLabel( 'ID' )->setWidth( '10px' ),
            AdminColumn::text( 'name' )->setLabel( 'Название' )->setWidth( '100px' ),
            AdminColumn::link( 'src' )->setLabel( 'Ссылка' )->setWidth( '100px' ),
            AdminColumnEditable::checkbox('moderate')->setLabel('Модерация')
        )->paginate( 10 );
        return $display;
    } );
    $model->onCreateAndEdit( function ( $id = null )
    {
        $panel = AdminForm::panel()
            ->addBody(
                AdminFormElement::text( 'src', 'Ссылка' ),
                AdminFormElement::text( 'name', 'Название' ),
                AdminFormElement::checkbox  ( 'moderate', 'Пропускать без модерации' )
            )
        ;
        $panel ->getButtons()->hideDeleteButton();
        return $panel ;
    } );
} );