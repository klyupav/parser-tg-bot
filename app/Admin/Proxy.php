<?php

use App\Models\Proxy;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel( Proxy::class, function ( ModelConfiguration $model )
{
    $model->setTitle( 'Прокси' );
    $model->onDisplay( function ()
    {
        $display = AdminDisplay::datatablesAsync()->setHtmlAttribute( 'class', 'table-primary table-hover' );
        $display->setColumns(
            AdminColumn::text( 'id' )->setLabel( 'ID' )->setWidth( '10px' ),
            AdminColumn::link( 'address' )->setLabel( 'Адрес:порт' )->setWidth( '100px' ),
            AdminColumn::text( 'user' )->setLabel( 'Логин' )->setWidth( '100px' ),
            AdminColumn::text( 'pass' )->setLabel( 'Пароль' )->setWidth( '100px' )
        )->paginate( 10 );
        return $display;
    } );
    $model->onCreateAndEdit( function ( $id = null )
    {
        $panel = AdminForm::panel()
            ->addBody(
                AdminFormElement::text( 'address', 'Адрес:порт' ),
                AdminFormElement::text( 'user', 'Логин' ),
                AdminFormElement::text( 'pass', 'Пароль' )
            );
        $panel ->getButtons()->hideDeleteButton();
        return $panel ;
    } );
} );