<?php

namespace App\Jobs\ParseAllSources;


use App\Donors\Instagram;
use App\Donors\InstagramAccount;
use Illuminate\Support\Facades\Cache;

class InstagramJob extends AbstractJob
{
    protected $threadName;

    public function __construct($procesNum = 1)
    {
        parent::__construct();
        $procesName = Instagram::getInstance()->getType();
        $this->threadName = "parse-{$procesName}-{$procesNum}";
        $this->onQueue($this->threadName);
    }

    public function handle()
    {
        $startTime = now();

        $this->debug("{$this->threadName} start");

        $sources = Cache::get("{$this->threadName}-".Variables::$keySourcesCache);
        $accounts = Cache::get("{$this->threadName}-".Variables::$keyAccountsCache);

        $instagram = new Instagram(new InstagramAccount($accounts));
        foreach ($sources as $source)
        {
            $instagram->getPosts($source->src);
            sleep(60);
        }

        $this->debug("{$this->threadName} finish, time: " . now()->diffForHumans($startTime));
    }
}
