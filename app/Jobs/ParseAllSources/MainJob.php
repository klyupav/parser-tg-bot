<?php

namespace App\Jobs\ParseAllSources;


use App\Donors\Instagram;
use App\Donors\InstagramAccount;
use App\Donors\Youtube;
use App\Models\Source;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class MainJob extends AbstractJob
{

    public function handle()
    {
        $this->debug('start');

        $this->getChainSources();

        $this->debug('finish');
    }

    private function getChainSources()
    {
        $typeSources = Source::getAllTypeSources();
        foreach ($typeSources as $typeSource)
        {
            $procesName = $typeSource->from;

//            if ($typeSource->from != "Youtube")
//            {
//                continue;
//            }

            $sourcesByType = Source::getAllSourcesByType($typeSource->from);

            if ($sourcesByType->isEmpty())
            {
                continue;
            }

            $threadCount = env("THREADS_" . strtoupper($typeSource->from), 1);

//            $threadCount = InstagramAccount::getAllWorks()->count();
            $splitedSources = $sourcesByType->split($threadCount);

            $splitedAccounts = $this->getSplitedAccountsOrProxyByThreads($procesName, $threadCount);

            if (!$splitedAccounts->isEmpty())
            {
                if ($splitedSources->count() > $splitedAccounts->count() )
                {
                    $this->debug("Not enough accounts for {$procesName}. Splited Sources = {$splitedSources->count()} > Splited accounts = {$splitedAccounts->count()}");
                }
            }

            foreach ($splitedSources as $k => $threadSources)
            {
                $procesNum = $k+1;
                $threadName = "parse-{$procesName}-{$procesNum}";
                if ($splitedAccounts->isEmpty())
                {
                    Cache::put("{$threadName}-".Variables::$keySourcesCache, $threadSources, now()->addHour(2));
                    Cache::put("{$threadName}-".Variables::$keyAccountsCache, null, now()->addHour(2));
                }
                else
                {
                    Cache::put("{$threadName}-".Variables::$keySourcesCache, $threadSources, now()->addHour(2));
                    Cache::put("{$threadName}-".Variables::$keyAccountsCache, $splitedAccounts->shift(), now()->addHour(2));
                }

                $this->generateThreads($procesName, $procesNum);
            }
        }
    }

    private function generateThreads($procesName, $procesNum)
    {
        switch ($procesName)
        {
            case class_basename(Instagram::class) :
                dispatch(new InstagramJob($procesNum));
                break;
            case class_basename(Youtube::class) :
                dispatch(new YoutubeJob($procesNum));
                break;
        }
    }

    private function getSplitedAccountsOrProxyByThreads(string $procesName, int $threadCount)
    {
        switch ($procesName)
        {
            case class_basename(Instagram::class) :
                $instagramAccounts = InstagramAccount::getAllWorks();
                if ($instagramAccounts->count() < $threadCount)
                {
                    //UPDATE `instagram_account` SET `ban`=0
                    throw new \Exception("Not enough users for all threads");
                }

                if ($instagramAccounts->isEmpty())
                {
                    return new Collection();
                }
                $chunkSize = floor($instagramAccounts->count() / $threadCount);
                $splitedAccounts = $instagramAccounts->chunk($chunkSize);
                return $splitedAccounts;
                break;

            case class_basename(Youtube::class) :
                return new Collection();
                break;
        }

        return null;
    }

    /**
     * @return bool
     */
    public static function isQueued()
    {
        if (DB::table(config('queue')['connections']['database']['table'])
                ->where('queue', 'LIKE', "parse-%")
                ->count() > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * @return \Illuminate\Support\Collection|null
     */
    public static function getAllThreads()
    {
        if (
            $jobs = DB::table(config('queue')['connections']['database']['table'])
                ->where('queue', 'LIKE', "parse-%")->groupBy(['queue'])->get(['queue'])
        )
        {
            if ($jobs->count() > 0)
            {
                return $jobs;
            }
        }

        return null;
    }
}
