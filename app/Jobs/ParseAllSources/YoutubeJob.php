<?php

namespace App\Jobs\ParseAllSources;


use App\Donors\Instagram;
use App\Donors\InstagramAccount;
use App\Donors\Youtube;
use Illuminate\Support\Facades\Cache;

class YoutubeJob extends AbstractJob
{
    protected $threadName;

    public function __construct($procesNum = 1)
    {
        parent::__construct();
        $procesName = Youtube::getInstance()->getType();
        $this->threadName = "parse-{$procesName}-{$procesNum}";
        $this->onQueue($this->threadName);
    }

    public function handle()
    {

        $startTime = now();

        $this->debug("{$this->threadName} start");

        $sources = Cache::get("{$this->threadName}-".Variables::$keySourcesCache);
//        $accounts = Cache::get("{$this->threadName}-".Variables::$keyAccountsCache);

        $donor = new Youtube();
        foreach ($sources as $source)
        {
            $donor->getPosts($source->src);
        }

        $this->debug("{$this->threadName} finish, time: " . now()->diffForHumans($startTime));
    }
}
