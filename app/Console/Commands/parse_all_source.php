<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Donors\GeneralDonor;
use App\Models\Source;

class parse_all_source extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse_all_source';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $generalDonor = new GeneralDonor();
        foreach (Source::all() as $source)
        {
            $posts = $generalDonor->getPosts($source->src);
            sleep(20);
        }
    }
}
