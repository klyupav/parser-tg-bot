<?php

namespace App\Console\Commands;

use App\Models\Chanels;
use App\Models\Post;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;

class send_notification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send_notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send telegram notofication';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $posts = Post::where('have_post', 0)->where('artisan', 0)->get();
        $url = 'https://api.telegram.org/bot1098425801:AAF_hoWvzexK2aiZTUfp2NyV1K191JX4heM/sendMessage';
        $url_1 = 'https://api.telegram.org/bot1098425801:AAF_hoWvzexK2aiZTUfp2NyV1K191JX4heM/sendPhoto';
        $url_2 = 'https://api.telegram.org/bot1098425801:AAF_hoWvzexK2aiZTUfp2NyV1K191JX4heM/sendMediaGroup';
        foreach ($posts as $post) {
            $publication = Carbon::parse($post->publication)->lt(Carbon::now()->addHours(3));
            $send_post = 0;
            if($publication) {
                $send_post = 1;
            }elseif ($post->publication == null) {
                $send_post = 1;
            }
            $source= $post->source;
            if ($source->from == 'Youtube' && $post->have_post == 0 && $send_post == 1)
            {
                foreach ($post->source->chanels as $chanel)
                {
                    $params = [
                        'chat_id' => $chanel->channel_id,
                        'text' => $post->src
                    ];
                    if ($post->source->moderate == 1){
                        $this->send_request_get($url, $params);
                    }

                }
//                $this->updatePost($post->id);
                $post->have_post = 1;
                $post->save();
            }elseif ($post->have_post == 0 && $send_post == 1 )
            {
                $images = explode('|', $post->attachments);
                $description = str_replace('@',' ',$post->description);
                $media = (object)[];
                $b = [];
                if (isset($images) && !empty($images)) {
                    foreach ($images as $key => $image) {
                        $content = explode("?", $image);
                        $content_t =  explode(".", $content[0]);
                        $content_type = end($content_t);
                        $photo = ['jpg', 'png', 'JPG', 'gif'];
                        if (in_array($content_type, $photo)){
                            if ($key == 0) {
                                if (isset($description) && !empty($description)){
                                    $a = new $media;
                                    $a->type = 'photo';
                                    $a->caption = Str::limit($description, 1000);
                                    $a->media = $image;
                                    $b[] = $a;
                                }else{
                                    $a = new $media;
                                    $a->type = 'photo';
                                    $a->media = $image;
                                    $b[] = $a;
                                }

                            } else {
                                $a = new $media;
                                $a->type = 'photo';
                                $a->media = $image;
                                $b[] = $a;
                            }
                        }else{
                            if ($key == 0) {
                                $a = new $media;
                                $a->type = 'video';
                                $a->caption = Str::limit($description, 1000);
                                $a->media = $image;
                                $b[] = $a;
                            } else {
                                $a = new $media;
                                $a->type = 'video';
                                $a->media = $image;
                                $b[] = $a;
                            }
                        }


                    }

                    foreach ($post->source->chanels as $chanel)
                    {

                        $params = [
                            'chat_id' => $chanel->channel_id,
                            'media' => json_encode($b)
                        ];
                        if ($post->source->moderate == 1){
                            $this->send_request_get($url_2, $params);
                        }

                    }
                }
                $post->have_post = 1;
                $post->save();
            }
        }

    }

    public function updatePost($id)
    {
        \DB::table('post')
            ->where('id', $id)
            ->update(['have_post' => 1]);
        sleep(20);
    }
    public function send_request_get($url, $params)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
    }
}
