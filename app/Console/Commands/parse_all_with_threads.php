<?php

namespace App\Console\Commands;

use App\Donors\InstagramAccount;
use App\Jobs\ParseAllSources\MainJob;
use Illuminate\Console\Command;
use Symfony\Component\Process\Process;

class parse_all_with_threads extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse_all_with_threads';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!MainJob::isQueued())
        {
            dispatch_now(new MainJob());
        }

        $jobs = MainJob::getAllThreads();
        $timeout = 60*40;
        foreach ($jobs as $job)
        {
            $cmd = 'php ' . base_path('artisan') . " queue:listen --queue={$job->queue} --tries=1 --timeout={$timeout}";
            $cmd_run_ifNotExist = "bash runCmdIfNotExist '{$cmd}'";
            print $cmd_run_ifNotExist."\n";
//            $process = new Process($cmd_run_ifNotExist);
//            $process->setTimeout($timeout);
//            $process->disableOutput();
//            $process->start();
        }
    }
}
