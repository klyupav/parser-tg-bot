<?php

namespace App\Console\Commands;

use App\Models\InstagramAccount;
use App\Models\Proxy;
use App\Models\Source;
use Illuminate\Console\Command;

class insert_proxy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insert_proxy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert two instagram accounts with proxy';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (Source::all() as $source)
        {
            if (preg_match("%instagram\.com/([^\/]+)/%uis", $source->src, $match))
            {
                $source->update(['name' => $match[1]]);
            }
        }
//        $proxy1 = Proxy::create(['address' => '194.38.10.99:45785']);
//        $proxy2 = Proxy::create(['address' => '92.63.195.186:45785', 'user' => 'SelSeobhdn', 'pass' => 'S1o4DyM']);
//        InstagramAccount::create([
//            'login' => 'beregoff.v@gmail.com',
//            'pass' => 'Jhdh7738sjhd',
//            'proxy_id' => $proxy1->id
//        ]);
//        InstagramAccount::create([
//            'login' => 'yakrivoruk@yandex.ru',
//            'pass' => 'hasfkjhsajkd908797)(&)(*90218930shdksad',
//            'proxy_id' => $proxy2->id
//        ]);
    }
}
