<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Chanels;
use App\Models\Post;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;

class send_artisan_notification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send_artisan_notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send artisan telegram notofication';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $posts = Post::where('artisan', 1)->get();
        $url = 'https://api.telegram.org/bot1098425801:AAF_hoWvzexK2aiZTUfp2NyV1K191JX4heM/sendMessage';
        $url_1 = 'https://api.telegram.org/bot1098425801:AAF_hoWvzexK2aiZTUfp2NyV1K191JX4heM/sendPhoto';
        $url_2 = 'https://api.telegram.org/bot1098425801:AAF_hoWvzexK2aiZTUfp2NyV1K191JX4heM/sendMediaGroup';

        foreach ($posts as $post) {
            $publ = Carbon::parse($post->publication)->addMinutes(60 * $post->time)->lt(Carbon::now('Europe/Moscow'));
            $publication = false;
            if ($publ)
            {
                if ($post->post_every == 1){
                    $publication = true;
                }elseif ($post->post_via == 1 && $post->count_post == 0)
                {
                    $publication = true;
                }
            }
            if ($publication)
            {
                $images = explode('|', $post->attachments);
                $description = str_replace('@',' ',$post->description);
                $media = (object)[];
                $b = [];
                if (isset($images) && !empty($images)) {
                    foreach ($images as $key => $image) {
                        $content = explode("?", $image);
                        $content_t =  explode(".", $content[0]);
                        $content_type = end($content_t);
                        $photo = ['jpg', 'png', 'JPG', 'gif'];
                        if (in_array($content_type, $photo)){
                            if ($key == 0) {
                                if (isset($description) && !empty($description)){
                                    $a = new $media;
                                    $a->type = 'photo';
                                    $a->caption = Str::limit($description, 1000);
                                    $a->media = $image;
                                    $b[] = $a;
                                }else{
                                    $a = new $media;
                                    $a->type = 'photo';
                                    $a->media = $image;
                                    $b[] = $a;
                                }

                            } else {
                                $a = new $media;
                                $a->type = 'photo';
                                $a->media = $image;
                                $b[] = $a;
                            }
                        }else{
                            if ($key == 0) {
                                $a = new $media;
                                $a->type = 'video';
                                $a->caption = Str::limit($description, 1000);
                                $a->media = $image;
                                $b[] = $a;
                            } else {
                                $a = new $media;
                                $a->type = 'video';
                                $a->media = $image;
                                $b[] = $a;
                            }
                        }


                    }

                    foreach ($post->source->chanels as $chanel)
                    {

                        $params = [
                            'chat_id' => $chanel->channel_id,
                            'media' => json_encode($b)
                        ];
                        if ($post->source->moderate == 1){
                            $this->send_request_get($url_2, $params);
                        }

                    }
                }else{
                    foreach ($post->source->chanels as $chanel)
                    {
                        $params = [
                            'chat_id' => $chanel->channel_id,
                            'text' => $description
                        ];
                        $this->send_request_get($url, $params);

                    }
                }
                $post->publication = Carbon::now();
                $post->have_post = 1;
                $post->count_post += 1;
                $post->save();
            }
        }

    }
    public function send_request_get($url, $params)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
    }
}
