<?php

namespace App\Http\Controllers;

use Alaouy\Youtube\Facades\Youtube;
use Alaouy\Youtube\Tests\YoutubeTest;
use App\Donors\GeneralDonor;
use App\Donors\Instagram;
use App\Jobs\ParseAllSources\InstagramJob;
use App\Jobs\ParseAllSources\MainJob;
use App\Jobs\ParseAllSources\Variables;
use App\Models\Post;
use App\Models\Source;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class ParseitController extends Controller
{
    public function parseYoutube()
    {
//        if (!MainJob::isQueued())
//        {
//            dispatch_now(new MainJob());
//        }
//        dd('end');

        $donor = new \App\Donors\Youtube();
        foreach (Source::whereFrom('Youtube')->get() as $source)
        {
            $donor->getPosts($source->src);
        }

    }

    //php artisan queue:listen --queue=parse-all --tries=3
    public function parseAllSourcesJob()
    {
        $sources = Cache::get("parse-Instagram-4-".Variables::$keySourcesCache);
        $accounts = Cache::get("parse-Instagram-4-".Variables::$keyAccountsCache);
        dd([$sources, $accounts]);
//        if (!MainJob::isQueued())
//        {
//            $job = new MainJob();
//            $job->handle();
////            MainJob::dispatch();
//        }
//        $job = new InstagramJob(1);
//        $job->handle();
    }

    public function parseInstagramBySourceIdAndAccountId(Request $request, $sourceId, $accountId)
    {
        $account = \App\Models\InstagramAccount::find($accountId);
        $instagramParser = new Instagram(null, $account);
        if ($source = Source::find($sourceId))
        {
            $posts = $instagramParser->getPosts($source->src);
            print_r($posts);
        }
    }

    public function parsePostsBySourceId(Request $request, $id)
    {
        $generalDonor = new GeneralDonor();
        if ($source = Source::find($id))
        {
            $posts = $generalDonor->getPosts($source->src);
            print_r($posts);
        }
    }

    public function parsePostsFromAllSources(Request $request)
    {
        $generalDonor = new GeneralDonor();
        $i = 0;
        foreach (Source::all() as $source)
        {
            $i++;
            if ($i>4)
            {
                break;
            }
            $posts = $generalDonor->getPosts($source->src);
            print_r($posts);
            sleep(2);
//            break;
        }

    }

    public function index()
    {
        dd('BOBOIL');
        $post = Post::find(1725);
        $description = str_replace('@',' ',$post->description);
        $publication = Carbon::parse($post->publication)->lt(Carbon::now()->addHours(3));
        $images = explode('|', $post->attachments);
        dd($images);
        $posts = Post::all();
            $url = 'https://api.telegram.org/bot1098425801:AAF_hoWvzexK2aiZTUfp2NyV1K191JX4heM/sendMessage';
            $url_1 = 'https://api.telegram.org/bot1098425801:AAF_hoWvzexK2aiZTUfp2NyV1K191JX4heM/sendPhoto';
            $url_2 = 'https://api.telegram.org/bot1098425801:AAF_hoWvzexK2aiZTUfp2NyV1K191JX4heM/sendMediaGroup';

            foreach ($posts as $post) {
                $post = Post::find(54);

                if ($post->have_post == 0) {

//                    $images = json_decode($post->attachments);

                    $media = (object)[];
                    $b = [];
                    if (isset($images) && !empty($images)) {
//                        dd($images);
                        foreach ($images as $key => $image) {
                            $content = explode("?", $image);
                            $content_t =  explode(".", $content[0]);
                            $content_type = end($content_t);
                            $photo = ['jpg', 'png', 'JPG', 'gif'];
//                            dd($content_type);
                            if (in_array($content_type, $photo)){
                                if ($key == 0 && isset($post->description) && !empty($post->description)) {
                                    $a = new $media;
                                    $a->type = 'photo';
                                    $a->caption = Str::limit($post->description, 1000);
                                    $a->media = $image;
                                    $b[] = $a;
                                } else {
                                    $a = new $media;
                                    $a->type = 'photo';
                                    $a->media = $image;
                                    $b[] = $a;
                                }
                            }else{
                                if ($key == 0) {
                                    $a = new $media;
                                    $a->type = 'video';
                                    $a->caption = Str::limit($post->description, 1000);
                                    $a->media = $image;
                                    $b[] = $a;
                                } else {
                                    $a = new $media;
                                    $a->type = 'video';
                                    $a->media = $image;
                                    $b[] = $a;
                                }
                            }


                        }

                        foreach ($post->source->chanels as $chanel)
                        {
                            $params = [
                                'chat_id' => $chanel->channel_id,
                                'media' => json_encode($b)
                            ];
                            if ($post->source->moderate == 1){
                                $this->send_request_get($url_2, $params);
                            }

                        }
                    }
                    \DB::table('post')
                        ->where('id', $post->id)
                        ->update(['have_post' => 1]);
                    sleep(20);
                }
            }
    }

    public function send_request_get($url, $params) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
//        print_r($response);die();
    }

    public function postNow(Request $request)
    {
        $url_2 = 'https://api.telegram.org/bot1098425801:AAF_hoWvzexK2aiZTUfp2NyV1K191JX4heM/sendMediaGroup';
        $url = 'https://api.telegram.org/bot1098425801:AAF_hoWvzexK2aiZTUfp2NyV1K191JX4heM/sendMessage';
        $post = Post::find($request->input('id'));
        $publication = Carbon::parse($post->publication)->lt(Carbon::now());
            $images = explode('|', $post->attachments);
            $description = str_replace('@',' ',$post->description);
            $media = (object)[];
            $b = [];
            $source= $post->source;
        if ($source->from == 'Youtube')
        {
            foreach ($post->source->chanels as $chanel)
            {

                $params = [
                    'chat_id' => $chanel->channel_id,
                    'text' => $post->src
                ];
                if ($post->source->moderate == 1){
                    $this->send_request_get($url, $params);
                }

            }
        } elseif (isset($post->attachments) && !empty($post->attachments)) {
                foreach ($images as $key => $image) {
                    $content = explode("?", $image);
                    $content_t =  explode(".", $content[0]);
                    $content_type = end($content_t);
                    $photo = ['jpg', 'png', 'JPG', 'gif'];
                    if (in_array($content_type, $photo)){
                        if ($key == 0 && isset($description) && !empty($description)) {
                            $a = new $media;
                            $a->type = 'photo';
                            $a->caption = Str::limit($description, 1000);
                            $a->media = $image;
                            $b[] = $a;
                        } else {
                            $a = new $media;
                            $a->type = 'photo';
                            $a->media = $image;
                            $b[] = $a;
                        }
                    }else{
                        if ($key == 0) {
                            $a = new $media;
                            $a->type = 'video';
                            $a->caption = Str::limit($description, 1000);
                            $a->media = $image;
                            $b[] = $a;
                        } else {
                            $a = new $media;
                            $a->type = 'video';
                            $a->media = $image;
                            $b[] = $a;
                        }
                    }

                }

                foreach ($post->source->chanels as $chanel)
                {
                    $params = [
                        'chat_id' => $chanel->channel_id,
                        'media' => json_encode($b)
                    ];
                    $this->send_request_get($url_2, $params);

                }
            }else{
                foreach ($post->source->chanels as $chanel)
                {
                    $params = [
                        'chat_id' => $chanel->channel_id,
                        'text' => $description
                    ];
                    $this->send_request_get($url, $params);

                }
            }

    }
    public function postArtisanNow(Request $request)
    {
        $url_2 = 'https://api.telegram.org/bot1098425801:AAF_hoWvzexK2aiZTUfp2NyV1K191JX4heM/sendMediaGroup';
        $url = 'https://api.telegram.org/bot1098425801:AAF_hoWvzexK2aiZTUfp2NyV1K191JX4heM/sendMessage';
        $post = Post::find($request->input('id'));
        $images = explode('|', $post->attachments);
        $description = str_replace('@',' ',$post->description);
        $media = (object)[];
        $b = [];
        $source= $post->source;
        if (isset($post->attachments) && !empty($post->attachments)) {

            foreach ($images as $key => $image) {
                $content = explode("?", $image);
                $content_t =  explode(".", $content[0]);
                $content_type = end($content_t);
                $photo = ['jpg', 'png', 'JPG', 'gif'];

                if (in_array($content_type, $photo)){
                    if ($key == 0 && isset($description) && !empty($description)) {
                        $a = new $media;
                        $a->type = 'photo';
                        $a->caption = Str::limit($description, 1000);
                        $a->media = $image;
                        $b[] = $a;
                    } else {
                        $a = new $media;
                        $a->type = 'photo';
                        $a->media = $image;
                        $b[] = $a;
                    }
                }else{
                    if ($key == 0) {
                        $a = new $media;
                        $a->type = 'video';
                        $a->caption = Str::limit($description, 1000);
                        $a->media = $image;
                        $b[] = $a;
                    } else {
                        $a = new $media;
                        $a->type = 'video';
                        $a->media = $image;
                        $b[] = $a;
                    }
                }

            }

            foreach ($post->source->chanels as $chanel)
            {
                $params = [
                    'chat_id' => $chanel->channel_id,
                    'media' => json_encode($b)
                ];
                $this->send_request_get($url_2, $params);

            }
        }else{
            foreach ($post->source->chanels as $chanel)
            {
                $params = [
                    'chat_id' => $chanel->channel_id,
                    'text' => $description
                ];
                $this->send_request_get($url, $params);

            }
        }
    }
}