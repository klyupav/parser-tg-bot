<button onclick="postMessageNow('{{$model->id}}')" class="">запостить сейчас</button>
<script>
    function postMessageNow(id) {
        var token = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            method: "POST",
            url: "/post-now",
            data: { id: id, _token: token }
        })
            .done(function( msg ) {
                alert( "Опубликовано " + msg );
            });
    }
</script>