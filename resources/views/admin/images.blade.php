<?php  $images = explode('|', $model->attachments); ?>
    @if(isset($images) && !empty($images))
        @foreach($images as $image)
            <?php
                $content = explode("?", $image);
                $content_t =  explode(".", $content[0]);
                $content_type = end($content_t);
                $photo = ['jpg', 'png'];
            ?>
            @if(in_array($content_type, $photo))
                <img src="{{$image}}" width="150px"/>
            @else
                <video controls="controls">
                    <source src="{{$image}}">
                </video>
            @endif
        @endforeach
    @endif

