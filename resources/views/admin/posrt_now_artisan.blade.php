<button onclick="postArtisanMessageNow('{{$model->id}}')" class="">запостить сейчас</button>
<script>
    function postArtisanMessageNow(id) {
        var token = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            method: "POST",
            url: "/artisan-post-now",
            data: { id: id, _token: token }
        })
            .done(function( msg ) {
                alert( "Опубликовано " + msg );
            });
    }
</script>